import ROOT
import numpy as np




ROOT.gROOT.LoadMacro("/eos/home-m/mhuwiler/plugins/libFunctions.C")


precision = 5
rangeLowerBound = 0. 
rangeUpperBound = 20.


# Getting the files 
filemanager = ROOT.FileManager()

filemanager.AddItem("mc", "/eos/home-m/mhuwiler/data/added/PhotonID/mc_SingleElectron_RunIIFall17-3_1_X-v1_merged_EB_MVAidXGBOPuppi.root", "zeevalidation_13TeV_All")
filemanager.AddItem("data", "/eos/home-m/mhuwiler/data/added/PhotonID/data_SingleElectron_RunIIFall17-3_1_X-v1_All_EB_MVAidXGBOPuppi.root", "zeevalidation_13TeV_All")

filemanager.OpenAllItems(); 



mybins = [0, 2, 3, 5, 7, 20]

bins = ROOT.CustomBins(precision, rangeLowerBound, rangeUpperBound)

superbins = ROOT.CustomBins(len(mybins), np.asarray(mybins, 'd'))

binvector = ROOT.vector("Double_t")()
for i in mybins: 
	binvector.push_back(i)
	
crazybins = ROOT.CustomBins(binvector)

try : 
	custombins = ROOT.customBins()
except AttributeError : 
	print "The default constructor is properly disabled! "

h1 = ROOT.TH1D("h1", "h1", bins.GetNBins(), bins.GetBinEdges())
h2 = ROOT.TH1D("h2", "h2", superbins.GetNBins(), superbins.GetBinEdges())
h3 = ROOT.TH1D("h3", "h3", crazybins.GetNBins(), crazybins.GetBinEdges())
h4 = ROOT.TH1D("h4", "h4", len(mybins)-1, np.asarray(mybins, 'd'))

signalmc = filemanager.GetItem("mc")
signaldata = filemanager.GetItem("data")

histocanvas = ROOT.TCanvas("histocanvas", "histocanvas", 800, 600)
histocanvas.Divide(2, 2)
histocanvas.cd(1)
signaldata.Draw("rho>>h1")

histocanvas.cd(2)
signaldata.Draw("rho>>h2")

histocanvas.cd(3)
signaldata.Draw("rho>>h3")

histocanvas.cd(4)
signaldata.Draw("rho>>h4")

histocanvas.Modified()
histocanvas.Update()



#rep = ''
#while not rep in [ 'q', 'Q' ]:
#   rep = raw_input( 'enter "q" to quit: ' )
#   if 1 < len(rep):
#      rep = rep[0]

input("PRESS ENTER TO CONTINUE.")

filemanager.Clear()
